package com.futurerx.jep;

import java.lang.reflect.Field;

import org.apache.commons.jxpath.AbstractFactory;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;

public class JxpathObjectFactory extends AbstractFactory {
	public boolean createObject(JXPathContext context, Pointer pointer,
			Object parent, String name, int index) {
		try {
			Class parentClass = parent.getClass();
			Field f = parentClass.getDeclaredField(name);
			f.setAccessible(true);
			Object o = f.get(parent);
			if (o == null) {
				Class fClass = f.getType();
				f.set(parent, fClass.newInstance());
//				System.out.println("TRUE:" + parent + ":" + name);
				return true;
			}
//			System.out.println("FALSE");
			return false;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
