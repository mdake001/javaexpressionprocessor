package com.futurerx.jep;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.nfunk.jep.JEP;
import org.nfunk.jep.Node;

import com.futurerx.jep.function.AddDatePart;
import com.futurerx.jep.function.Avg;
import com.futurerx.jep.function.Between;
import com.futurerx.jep.function.Concat;
import com.futurerx.jep.function.Contains;
import com.futurerx.jep.function.Count;
import com.futurerx.jep.function.CurrentDate;
import com.futurerx.jep.function.DateToStr;
import com.futurerx.jep.function.EndsWith;
import com.futurerx.jep.function.Equals;
import com.futurerx.jep.function.Execute;
import com.futurerx.jep.function.ExecuteSQL;
import com.futurerx.jep.function.ExecuteSQLUpdate;
import com.futurerx.jep.function.First;
import com.futurerx.jep.function.Gaps;
import com.futurerx.jep.function.GetData;
import com.futurerx.jep.function.GetDatePart;
import com.futurerx.jep.function.GetField;
import com.futurerx.jep.function.GetObjectData;
import com.futurerx.jep.function.GetSQLData;
import com.futurerx.jep.function.GreaterOrEqual;
import com.futurerx.jep.function.GreaterThan;
import com.futurerx.jep.function.If;
import com.futurerx.jep.function.IfNull;
import com.futurerx.jep.function.In;
import com.futurerx.jep.function.Invoke;
import com.futurerx.jep.function.IsNull;
import com.futurerx.jep.function.LPad;
import com.futurerx.jep.function.Last;
import com.futurerx.jep.function.Length;
import com.futurerx.jep.function.LengthBoolean;
import com.futurerx.jep.function.LessOrEqual;
import com.futurerx.jep.function.LessThan;
import com.futurerx.jep.function.Lower;
import com.futurerx.jep.function.Matches;
import com.futurerx.jep.function.MatchesCodeSet;
import com.futurerx.jep.function.Max;
import com.futurerx.jep.function.Min;
import com.futurerx.jep.function.New;
import com.futurerx.jep.function.Not;
import com.futurerx.jep.function.Null;
import com.futurerx.jep.function.Print;
import com.futurerx.jep.function.SetData;
import com.futurerx.jep.function.SetDatePart;
import com.futurerx.jep.function.SetField;
import com.futurerx.jep.function.SetObjectData;
import com.futurerx.jep.function.StartsWith;
import com.futurerx.jep.function.StrToDate;
import com.futurerx.jep.function.Substring;
import com.futurerx.jep.function.Sum;
import com.futurerx.jep.function.TitleCase;
import com.futurerx.jep.function.ToInt;
import com.futurerx.jep.function.ToRegexPattern;
import com.futurerx.jep.function.Upper;
import com.futurerx.jep.function.isValidDateFormat;

public class ExpressionProcessor {
	JEP jep = new JEP();
	Map<String, Node> parsedConditions = new HashMap<String, Node>();
	GetData getdata = null;
	SetData setdata = null;
	Connection connection = null;
	
	public void setContext(Context context) {
		this.getdata.setContext(context);
		this.setdata.setContext(context);
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public ExpressionProcessor(Context context, Connection connection)
	{
		this.connection = connection;
		this.jep.addStandardFunctions();
		this.jep.addStandardConstants();
		this.jep.addFunction("addDatePart", new AddDatePart());
		this.jep.addFunction("between", new Between());
		this.jep.addFunction("contains", new Contains());
		this.jep.addFunction("currentDate", new CurrentDate());
		this.jep.addFunction("dateToStr", new DateToStr());
		this.jep.addFunction("endsWith", new EndsWith());
		this.jep.addFunction("equals", new Equals());
		this.getdata = new GetData();
		this.setdata = new SetData();
		this.jep.addFunction("getData", this.getdata);
		this.jep.addFunction("setData", this.setdata);
		this.jep.addFunction("getDatePart", new GetDatePart());
		this.jep.addFunction("greaterOrEqual", new GreaterOrEqual());
		this.jep.addFunction("greaterThan", new GreaterThan());
		this.jep.addFunction("in", new In());
		this.jep.addFunction("isNull", new IsNull());
		this.jep.addFunction("lessOrEqual", new LessOrEqual());
		this.jep.addFunction("lessThan", new LessThan());
		this.jep.addFunction("lower", new Lower());
		this.jep.addFunction("matches", new Matches());
		this.jep.addFunction("not", new Not());
		this.jep.addFunction("setDatePart", new SetDatePart());
		this.jep.addFunction("startsWith", new StartsWith());
		this.jep.addFunction("strToDate", new StrToDate());
		this.jep.addFunction("substring", new Substring());
		this.jep.addFunction("upper", new Upper());
		this.jep.addFunction("sum", new Sum());
		this.jep.addFunction("avg", new Avg());
		this.jep.addFunction("max", new Max());
		this.jep.addFunction("min", new Min());
		this.jep.addFunction("count", new Count());
		this.jep.addFunction("first", new First());
		this.jep.addFunction("last", new Last());
		this.jep.addFunction("toInt", new ToInt());
		this.jep.addFunction("gaps", new Gaps());
		this.jep.addFunction("new", new New());
		this.jep.addFunction("invoke", new Invoke());
		this.jep.addFunction("getField", new GetField());
		this.jep.addFunction("setField", new SetField());
		this.jep.addFunction("null", new Null());
		this.jep.addFunction("toRegexPattern", new ToRegexPattern());
		this.jep.addFunction("matchesCodeSet", new MatchesCodeSet());
		this.jep.addFunction("length", new Length());
		this.jep.addFunction("lPad", new LPad());
		this.jep.addFunction("ifNull", new IfNull());
		this.jep.addFunction("if", new If());
		this.jep.addFunction("getObjectData", new GetObjectData());
		this.jep.addFunction("setObjectData", new SetObjectData());
		this.jep.addFunction("getSQLData", new GetSQLData(this.connection));
		this.jep.addFunction("titleCase", new TitleCase());
		this.jep.addFunction("print", new Print());
		this.jep.addFunction("concat", new Concat());
		this.jep.addFunction("execute", new Execute());
		this.jep.addFunction("executeSQLUpdate", new ExecuteSQLUpdate(this.connection));
		this.jep.addFunction("executeSQL", new ExecuteSQL(this.connection));
		this.jep.addFunction("isValidDateFormat", new isValidDateFormat());
		this.jep.addFunction("lengthBoolean", new LengthBoolean());
	}

//	public boolean evaluateCondition(String condition) throws Exception
//	{
//		Object returnValue = null;
//		String expression = "if(" + condition + ",\"true\",\"false\")";
//		
//		boolean isError = false;
//		this.jep.parseExpression(expression);
//
//		if (this.jep.hasError())
//		{
//			isError = true;
//			throw new RuntimeException(this.jep.getErrorInfo());
//		}
//		else
//		{
//			// expression is OK, get the value
//			Object expVal = this.jep.getValueAsObject();
//
//			// did error occur during evaluation?
//			if (this.jep.hasError())
//			{
//				isError = true;
//				throw new RuntimeException(this.jep.getErrorInfo());
//			}
//			else
//			{
//				returnValue = expVal;
//			}
//		}
//		if (!isError)
//		{
//			if (returnValue != null && returnValue.toString().equals("true"))
//			{
//				return true;
//			}
//			else
//			{
//				return false;
//			}
//		}
//		else
//		{
//			return false;
//		}
//	}

	public boolean evaluateCondition(String condition) throws Exception
	{
		Object returnValue = null;
		String expression = "if(" + condition + ",\"true\",\"false\")";

		boolean isError = false;
		
		Node n = null;
		if (parsedConditions.containsKey(condition)) {
			n = parsedConditions.get(condition);
		}
		else {
			n = this.jep.parse(expression);
			parsedConditions.put(condition, n);
		}
		
		if (this.jep.hasError())
		{
			isError = true;
			throw new RuntimeException(this.jep.getErrorInfo());
		}
		else
		{
			// expression is OK, get the value
			Object expVal = this.jep.evaluate(n);

			// did error occur during evaluation?
			if (this.jep.hasError())
			{
				isError = true;
				throw new RuntimeException(this.jep.getErrorInfo());
			}
			else
			{
				returnValue = expVal;
			}
		}
		if (!isError)
		{
			if (returnValue != null && returnValue.toString().equals("true"))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
//	public Object evaluateExpression(String expression) throws Exception
//	{
//		Object returnValue = null;
//		boolean isError = false;
//		this.jep.parseExpression(expression);
//
//		if (this.jep.hasError())
//		{
//			isError = true;
//			throw new RuntimeException(this.jep.getErrorInfo());
//		}
//		else
//		{
//			// expression is OK, get the value
//			Object expVal = this.jep.getValueAsObject();
//
//			// did error occur during evaluation?
//			if (this.jep.hasError())
//			{
//				isError = true;
//				throw new RuntimeException(this.jep.getErrorInfo());
//			}
//			else
//			{
//				returnValue = expVal;
//			}
//		}
//		return returnValue;
//	}
	public Object evaluateExpression(String expression) throws Exception
	{
		Object returnValue = null;
		boolean isError = false;
//		this.jep.parseExpression(expression);
		Node n = null;
		if (parsedConditions.containsKey(expression)) {
			n = parsedConditions.get(expression);
		}
		else {
			n = this.jep.parse(expression);
			parsedConditions.put(expression, n);
		}
		
		if (this.jep.hasError())
		{
			isError = true;
			throw new RuntimeException(this.jep.getErrorInfo());
		}
		else
		{
			// expression is OK, get the value
			Object expVal =  this.jep.evaluate(n);

			// did error occur during evaluation?
			if (this.jep.hasError())
			{
				isError = true;
				throw new RuntimeException(this.jep.getErrorInfo());
			}
			else
			{
				returnValue = expVal;
			}
		}
		return returnValue;
	}
}
