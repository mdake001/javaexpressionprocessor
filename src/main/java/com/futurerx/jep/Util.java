package com.futurerx.jep;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

public class Util {
	public static void setClassFieldValue(Field field, Object obj, Object val)
			throws Exception {
		try {
			field.setAccessible(true);
			field.set(obj, val);
		} catch (IllegalAccessException e) {
			String setter = "set"
					+ field.getName().substring(0, 1).toUpperCase()
					+ field.getName().substring(1);
			Class[] sig = new Class[1];
			Object[] args = new Object[1];
			sig[0] = field.getType();
			args[0] = val;

			Method refMethod = Util.getClassMethod(obj.getClass(), setter, sig);
			refMethod.setAccessible(true);
			refMethod.invoke(obj, args);
		}
	}

	public static Object getClassFieldValue(Field field, Object obj)
			throws Exception {
		try {
			field.setAccessible(true);
			return field.get(obj);
		} catch (IllegalAccessException e) {
			String getter = "get"
					+ field.getName().substring(0, 1).toUpperCase()
					+ field.getName().substring(1);
			Class[] sig = new Class[0];
			Object[] args = new Object[0];

			Method refMethod = Util.getClassMethod(obj.getClass(), getter, sig);
			refMethod.setAccessible(true);
			return refMethod.invoke(obj, args);
		}
	}

	public static Field getClassField(String className, String fieldName)
			throws Exception {
		ClassLoader loader = Util.class.getClassLoader();
		java.lang.reflect.Field refField = null;
		Class clazz = loader.loadClass(className);
		return Util.getClassField(clazz, fieldName);
	}

	public static Field getClassField(Class clazz, String fieldName)
			throws Exception {
		java.lang.reflect.Field refField = null;
		while (true) {
			try {
				refField = clazz.getDeclaredField(fieldName);
			} catch (NoSuchFieldException nsme) {
				clazz = clazz.getSuperclass();
				if (clazz == null) {
					throw new RuntimeException();
				}
				continue;
			}
			break;
		}
		return refField;
	}

	public static Method getClassMethod(String className, String methodName,
			Class[] sig) throws Exception {
		ClassLoader loader = Util.class.getClassLoader();
		java.lang.reflect.Method refMethod = null;
		Class clazz = loader.loadClass(className);
		return Util.getClassMethod(clazz, methodName, sig);
	}

	public static Method getClassMethod(Class clazz, String methodName,
			Class[] sig) throws Exception {
		java.lang.reflect.Method refMethod = null;
		while (true) {
			try {
				refMethod = clazz.getDeclaredMethod(methodName, sig);
			} catch (NoSuchMethodException nsme) {
				clazz = clazz.getSuperclass();
				if (clazz == null) {
					throw new RuntimeException();
				}
				continue;
			}
			break;
		}
		return refMethod;
	}

	public static Constructor getClassConstructor(String className, Class[] sig)
			throws Exception {
		ClassLoader loader = Util.class.getClassLoader();
		java.lang.reflect.Constructor refConstructor = null;
		Class clazz = loader.loadClass(className);
		return Util.getClassConstructor(clazz, sig);
	}

	public static Constructor getClassConstructor(Class clazz, Class[] sig)
			throws Exception {
		java.lang.reflect.Constructor refConstructor = null;
		while (true) {
			try {
				refConstructor = clazz.getDeclaredConstructor(sig);
			} catch (NoSuchMethodException nsme) {
				clazz = clazz.getSuperclass();
				if (clazz == null) {
					throw new RuntimeException();
				}
				continue;
			}
			break;
		}
		return refConstructor;
	}

	public static Object newInstance(Constructor cons, Object[] args)
			throws Exception {
		return cons.newInstance(args);
	}

	public static Class getPrimitiveTypeClass(String primitiveType) {
		Class clazz = null;
		if (primitiveType.equals("int")) {
			clazz = Integer.TYPE;
		} else if (primitiveType.equals("boolean")) {
			clazz = Boolean.TYPE;
		} else if (primitiveType.equals("char")) {
			clazz = Character.TYPE;
		} else if (primitiveType.equals("byte")) {
			clazz = Byte.TYPE;
		} else if (primitiveType.equals("short")) {
			clazz = Short.TYPE;
		} else if (primitiveType.equals("long")) {
			clazz = Long.TYPE;
		} else if (primitiveType.equals("float")) {
			clazz = Float.TYPE;
		} else if (primitiveType.equals("double")) {
			clazz = Double.TYPE;
		} else {
			clazz = null;
		}
		return clazz;
	}

	public static List convertToList(Object val) {
		if (val == null) {
			return null;
		}
		if (val instanceof Collection) {
			return new ArrayList((Collection) val);
		}
		if (val instanceof Map) {
			return new ArrayList(((Map) val).values());
		} else {
			List returnList = new ArrayList();
			returnList.add(val);
			return returnList;
		}
	}

	public static Object getFieldValueObject(Class type, Object val)
			throws Exception {
		ClassLoader loader = Util.class.getClassLoader();
		if (val == null) {
			return null;
		}

		String fieldTypeName = type.getName();

		String stringValue = val.toString();
		Object realValue = null;

		if (fieldTypeName.equals("int")
				|| fieldTypeName.equals("java.lang.Integer")) {
			if (val instanceof Number) {
				realValue = new Integer(((Number) val).intValue());
			} else {
				if (fieldTypeName.equals("java.lang.Integer")) {
					realValue = new Integer(stringValue);
				} else {
					realValue = new Integer(stringValue).intValue();
				}
			}
		} else if (fieldTypeName.equals("boolean")
				|| fieldTypeName.equals("java.lang.Boolean")) {
			realValue = new Boolean(stringValue);
		} else if (fieldTypeName.equals("char")
				|| fieldTypeName.equals("java.lang.Character")) {
			realValue = new Character(
					(char) new Integer(stringValue).intValue());
		} else if (fieldTypeName.equals("byte")
				|| fieldTypeName.equals("java.lang.Byte")) {
			realValue = new Byte(stringValue);
		} else if (fieldTypeName.equals("short")
				|| fieldTypeName.equals("java.lang.Short")) {
			if (val instanceof Number) {
				realValue = new Short(((Number) val).shortValue());
			} else {
				if (fieldTypeName.equals("java.lang.Short")) {
					realValue = new Short(stringValue);
				} else {
					realValue = new Short(stringValue).shortValue();
				}
			}
		} else if (fieldTypeName.equals("long")
				|| fieldTypeName.equals("java.lang.Long")) {
			if (val instanceof Number) {
				realValue = new Long(((Number) val).longValue());
			} else {
				if (fieldTypeName.equals("java.lang.Long")) {
					realValue = new Long(stringValue);
				} else {
					realValue = new Long(stringValue).longValue();
				}
			}
		} else if (fieldTypeName.equals("float")
				|| fieldTypeName.equals("java.lang.Float")) {
			if (val instanceof Number) {
				realValue = new Float(((Number) val).floatValue());
			} else {
				if (fieldTypeName.equals("java.lang.Float")) {
					realValue = new Float(stringValue);
				} else {
					realValue = new Float(stringValue).floatValue();
				}
			}
		} else if (fieldTypeName.equals("double")
				|| fieldTypeName.equals("java.lang.Double")) {
			if (val instanceof Number) {
				realValue = new Double(((Number) val).doubleValue());
			} else {
				if (fieldTypeName.equals("java.lang.Double")) {
					realValue = new Double(stringValue);
				} else {
					realValue = new Double(stringValue).doubleValue();
				}
			}
		} else if (fieldTypeName.equals("java.sql.Timestamp")
				&& val instanceof java.lang.String) {
			realValue = java.sql.Timestamp.valueOf(stringValue);
		} else {
			Object object = null;
			List values = null;
			{

				// CHANGED FOR RAW OBJECT RETURN
				if (val instanceof String
						|| ((val instanceof Collection) || val instanceof Map)) {
					values = Util.convertToList(val);
					if (fieldTypeName.startsWith("[I")) {
						object = Array.newInstance(Integer.TYPE, values.size());
					} else if (fieldTypeName.startsWith("[Z")) {
						object = Array.newInstance(Boolean.TYPE, values.size());
					} else if (fieldTypeName.startsWith("[C")) {
						object = Array.newInstance(Character.TYPE,
								values.size());
					} else if (fieldTypeName.startsWith("[B")) {
						object = Array.newInstance(Byte.TYPE, values.size());
					} else if (fieldTypeName.startsWith("[S")) {
						object = Array.newInstance(Short.TYPE, values.size());
					} else if (fieldTypeName.startsWith("[J")) {
						object = Array.newInstance(Long.TYPE, values.size());
					} else if (fieldTypeName.startsWith("[F")) {
						object = Array.newInstance(Float.TYPE, values.size());
					} else if (fieldTypeName.startsWith("[D")) {
						object = Array.newInstance(Double.TYPE, values.size());
					} else if (fieldTypeName.startsWith("[Ljava.lang.Integer")) {
						object = Array
								.newInstance(Integer.class, values.size());
					} else if (fieldTypeName.startsWith("[Ljava.lang.Boolean")) {
						object = Array
								.newInstance(Boolean.class, values.size());
					} else if (fieldTypeName
							.startsWith("[Ljava.lang.Character")) {
						object = Array.newInstance(Character.class,
								values.size());
					} else if (fieldTypeName.startsWith("[Ljava.lang.Byte")) {
						object = Array.newInstance(Byte.class, values.size());
					} else if (fieldTypeName.startsWith("[Ljava.lang.Short")) {
						object = Array.newInstance(Short.class, values.size());
					} else if (fieldTypeName.startsWith("[Ljava.lang.Long")) {
						object = Array.newInstance(Long.class, values.size());
					} else if (fieldTypeName.startsWith("[Ljava.lang.Float")) {
						object = Array.newInstance(Float.class, values.size());
					} else if (fieldTypeName.startsWith("[Ljava.lang.Double")) {
						object = Array.newInstance(Double.class, values.size());
					} else if (fieldTypeName.startsWith("[")) {
						String arrayElementClassName = null;
						{
							arrayElementClassName = fieldTypeName.substring(2)
									.replace(";", "");
						}
						Class clazz = loader.loadClass(arrayElementClassName);
						object = Array.newInstance(clazz, values.size());
					} else if (fieldTypeName.startsWith("java.util.Set")) {
						object = new HashSet();
					} else if (fieldTypeName.startsWith("java.util.SortedSet")) {
						object = new TreeSet();
					} else if (fieldTypeName.startsWith("java.util.List")) {
						object = new ArrayList(values);
					} else if (fieldTypeName.startsWith("java.util.Map")) {
						object = new HashMap();
					} else if (fieldTypeName.startsWith("java.util.SortedMap")) {
						object = new TreeMap();
					}
				} else {
					String elementClassName = null;
					{
						elementClassName = fieldTypeName;
					}

					{
						object = val;
						return object;
					}
				}
			}
			{
				Constructor refChildConstructor = null;
				Class childClazz = null;
				for (int i = 0; i < values.size(); i++) {
					Object value = values.get(i);
					String strVal = null;
					String key = null;
					Object objValue = null;

					{
						{
							if (value instanceof String) {
								strVal = (String) value;
							}
							objValue = strVal;
						}
					}
					{
						if (fieldTypeName.startsWith("[I")) {
							Array.set(object, i, new Integer(strVal).intValue());
						} else if (fieldTypeName.startsWith("[Z")) {
							Array.set(object, i,
									new Boolean(strVal).booleanValue());
						} else if (fieldTypeName.startsWith("[C")) {
							Array.set(object, i,
									new Character(strVal.charAt(0)).charValue());
						} else if (fieldTypeName.startsWith("[B")) {
							Array.set(object, i, new Byte(strVal).byteValue());
						} else if (fieldTypeName.startsWith("[S")) {
							Array.set(object, i, new Short(strVal).shortValue());
						} else if (fieldTypeName.startsWith("[J")) {
							Array.set(object, i, new Long(strVal).longValue());
						} else if (fieldTypeName.startsWith("[F")) {
							Array.set(object, i, new Float(strVal).floatValue());
						} else if (fieldTypeName.startsWith("[D")) {
							Array.set(object, i,
									new Double(strVal).doubleValue());
						} else if (fieldTypeName
								.startsWith("[Ljava.lang.Integer")) {
							Array.set(object, i, new Integer(strVal));
						} else if (fieldTypeName
								.startsWith("[Ljava.lang.Boolean")) {
							Array.set(object, i, new Boolean(strVal));
						} else if (fieldTypeName
								.startsWith("[Ljava.lang.Character")) {
							Array.set(object, i,
									new Character(strVal.charAt(0)));
						} else if (fieldTypeName.startsWith("[Ljava.lang.Byte")) {
							Array.set(object, i, new Byte(strVal));
						} else if (fieldTypeName
								.startsWith("[Ljava.lang.Short")) {
							Array.set(object, i, new Short(strVal));
						} else if (fieldTypeName.startsWith("[Ljava.lang.Long")) {
							Array.set(object, i, new Long(strVal));
						} else if (fieldTypeName
								.startsWith("[Ljava.lang.Float")) {
							Array.set(object, i, new Float(strVal));
						} else if (fieldTypeName
								.startsWith("[Ljava.lang.Double")) {
							Array.set(object, i, new Double(strVal));
						} else if (fieldTypeName.startsWith("[L")) {
							Array.set(object, i, objValue);
						} else if (fieldTypeName.startsWith("java.util.Map")) {
							((Map) object).put(key, objValue);
						} else if (object instanceof Vector) {
							((Vector) object).add(objValue);
						} else if (object instanceof Hashtable) {
							((Hashtable) object).put(key, objValue);
						} else if (object instanceof Set) {
							((Set) object).add(objValue);
						} else if (object instanceof List) {
							((List) object).add(objValue);
						} else if (object instanceof Map) {
							((Map) object).put(key, objValue);
						} else {
							object = objValue;
						}
					}
				}
				realValue = object;
			}
		}
		return realValue;
	}
}
