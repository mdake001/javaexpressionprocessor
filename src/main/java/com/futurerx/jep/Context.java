package com.futurerx.jep;

public class Context {
	Object obj = null;
	public Context(Object obj)
	{
		this.obj = obj;
	}
	
	public Object getObject()
	{
		return this.obj;
	}
	
	public void setContext(Object obj)
	{
		this.obj = obj;
	}
}
