/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.text.SimpleDateFormat;
import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class StrToDate extends PostfixMathCommand
{
	private Map<String, Integer> dateUnitMap = new HashMap<String, Integer>();
	public StrToDate()
	{
		numberOfParameters = 2;
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		checkStack(inStack); // check the stack
		Object param2 = inStack.pop();
		Object param1 = inStack.pop();
		Date retVal = null;
		
        if(param1 == null || param2 == null)
		{
			retVal = null;
		}
		else if(param1 instanceof String && param2 instanceof String)
		{
			String pattern = (String)param2;
		    SimpleDateFormat format = new SimpleDateFormat(pattern);
			try {
				retVal = format.parse((String)param1);
			} catch (java.text.ParseException e) {
				e.printStackTrace();
				throw new ParseException("Parse Exception.");
			}
		}
		else
		{
			throw new ParseException("Invalid parameters.");
		}
		inStack.push(retVal);
		return;
	}
}
