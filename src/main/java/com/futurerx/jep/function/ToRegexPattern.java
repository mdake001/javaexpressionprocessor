/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class ToRegexPattern extends PostfixMathCommand
{
	public ToRegexPattern()
	{
		numberOfParameters = 2;
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		checkStack(inStack); // check the stack
		Object param1 = null;
		Object param2 = null;
		{
			param2 = inStack.pop();
			param1 = inStack.pop();
			if((!(param1 instanceof String && param2 instanceof Number)) || (param1 == null || param2 == null))
			{
				throw new ParseException("Invalid parameters.");
			}
		}
		String retVal = null;
		retVal = this.getCodeRegex((String)param1, ((Number)param2).intValue());
		inStack.push(retVal);
		return;
	}

	private String getCodeRegex(String code, int length)
	{
		String retVal = null;
		boolean isPartial = (length > code.length());
		retVal = code.replaceAll("x", ".{1}");
		if(isPartial)
		{
			retVal = retVal + ".*";
		}
		//System.out.println("getCodeRegex:" + retVal);
		return retVal;
	}
}
