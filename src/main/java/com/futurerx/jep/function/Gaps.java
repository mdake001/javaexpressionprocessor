/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

 *****************************************************************************/

package com.futurerx.jep.function;

import java.lang.reflect.Field;
import java.util.*;

import org.apache.commons.lang3.time.DateUtils;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * The list function. Returns a Vector comprising all the children.
 * 
 * @author Rich Morris Created on 29-Feb-2004
 */
public class Gaps extends PostfixMathCommand {
	public Gaps() {
		numberOfParameters = -1;
	}

	public void run(Stack inStack) throws ParseException {
		int numParams = this.curNumberOfParameters;
		if (numParams < 3) {
			throw new ParseException(
					"'gaps' operator must have more than 2 arguments.");
		}
		checkStack(inStack); // check the stack
		List<Object> inList = new ArrayList<Object>();
		for (int i = 0; i < numParams - 5; i++) {
			Object param10 = inStack.pop();
			if (param10 == null) {
				continue;
			}
			if (param10 instanceof List) {
				inList.addAll((List) param10);
				continue;
			} else {
				inList.add(param10);
			}
		}
		Object anchorToDate = inStack.pop();
		Object anchorFromDate = inStack.pop();
		Object gapInDays = inStack.pop();
		Object toField = inStack.pop();
		Object fromField = inStack.pop();
		
		if(fromField == null || toField == null || gapInDays == null)
		{
			throw new ParseException(
			"Invalid parameters.");
		}
		if((!(fromField instanceof String)) || (!(toField instanceof String)) || (!(gapInDays instanceof Number)) || (anchorFromDate != null && !(anchorFromDate instanceof Date)) || (anchorToDate != null && !(anchorToDate instanceof Date)))
		{
			throw new ParseException(
			"Invalid parameters.");
		}
		List<Map<String, Date>> retVal = this.getDateRanges(inList, (String)fromField, (String)toField, (Date)anchorFromDate, (Date)anchorToDate);
		//System.out.println("getDateRanges:" + retVal);
		List<Map<String, Object>> gaps = this.getDateRangeGaps(retVal, ((Number)gapInDays).intValue(), null, null);
		//System.out.println("gaps:" + gaps);

		inStack.push(gaps);
		return;
	}
	
	private Object getField(Object obj, String fieldName) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		Object retVal = null;
		if(obj instanceof Map)
		{
			retVal = ((Map)obj).get(fieldName);
		}
		else
		{
			Class c = obj.getClass();
			Field f = c.getDeclaredField(fieldName);
			f.setAccessible(true);
			retVal = f.get(obj);
		}
		return retVal;
	}
	
	private List<Map<String, Object>> getDateRangeGaps(List<Map<String, Date>> dateList, int span, Date anchorStart, Date anchorEnd)
	{
		List<Map<String, Object>> retVal = new ArrayList<Map<String, Object>>();
		if(anchorStart != null)
		{
			anchorStart = DateUtils.truncate(anchorStart, Calendar.DATE);
		}
		if(anchorEnd != null)
		{
			anchorEnd = DateUtils.truncate(anchorEnd, Calendar.DATE);
		}
		Map<Date, Map<String, Date>> sortedDateList = new TreeMap<Date, Map<String, Date>>();
		for(int i = 0; i < dateList.size(); i++)
		{
			Map<String, Date> dateElement = dateList.get(i);
			if(!dateElement.containsKey("from"))
			{
				continue;
			}
			sortedDateList.put(dateElement.get("from"), dateElement);
		}
		Iterator<Map<String, Date>> sortedDateListIt = sortedDateList.values().iterator();
		Date previousDate = anchorStart;
		Date fromDate = null;
		Date toDate = null;
		while(sortedDateListIt.hasNext())
		{
			Map<String, Date> sortedDate = sortedDateListIt.next();
			fromDate = sortedDate.get("from");
			toDate = sortedDate.get("to");
			if(previousDate != null)
			{
				long dateDiffInMills = fromDate.getTime() - previousDate.getTime();
				long dateDiffInDays = dateDiffInMills/ (24 * 60 * 60 * 1000);
				if(dateDiffInDays >= span)
				{
					Map<String, Object> gapObj = new HashMap<String, Object>();
					gapObj.put("from", previousDate);
					gapObj.put("to", fromDate);
					gapObj.put("count", (int)dateDiffInDays);
					retVal.add(gapObj);
				}
			}
			previousDate = toDate;
		}
		if(anchorEnd != null)
		{
			long dateDiffInMills = anchorEnd.getTime() - previousDate.getTime();
			long dateDiffInDays = dateDiffInMills/ (24 * 60 * 60 * 1000);
			if(dateDiffInDays >= span)
			{
				Map<String, Object> gapObj = new HashMap<String, Object>();
				gapObj.put("from", previousDate);
				gapObj.put("to", anchorEnd);
				gapObj.put("count", (int)dateDiffInDays);
				retVal.add(gapObj);
			}
		}
		return retVal;
	}
	
	private List<Map<String, Date>> getDateRanges(List inList, String fromField, String toField, Date anchorStart, Date anchorEnd)
	{
		if(anchorStart != null)
		{
			anchorStart = DateUtils.truncate(anchorStart, Calendar.DATE);
		}
		if(anchorEnd != null)
		{
			anchorEnd = DateUtils.truncate(anchorEnd, Calendar.DATE);
		}
		List<Map<String, Date>> dateList = new ArrayList<Map<String, Date>>();
		int retVal = 0;
		String prevDataType = null;
		for(int i = 0; i < inList.size(); i++)
		{
			Map<String, Date> dateRangeMap = new HashMap<String, Date>();
			Object inElement = inList.get(i);
			Object fromDate = null;
			Object toDate = null;
			Date fromDateTruncated = null;
			Date toDateTruncated = null;
			try
			{
				fromDate = this.getField(inElement, fromField);
				toDate = this.getField(inElement, toField);
			}
			catch(Exception e)
			{
				throw new RuntimeException(
				e.getMessage());
			}
			if(fromDate == null || (!(fromDate instanceof Date)))
			{
				throw new RuntimeException(
				"Invalid parameters.");
			}
			fromDateTruncated = DateUtils.truncate((Date)fromDate, Calendar.DATE);
			if(toDate == null)
			{
				GregorianCalendar param1Cal = new GregorianCalendar();
				param1Cal.setTime(new Date());
				param1Cal.add(Calendar.YEAR, 10);
				toDate = param1Cal.getTime();
			}
			else
			{
				if((!(toDate instanceof Date)))
				{
					throw new RuntimeException(
					"Invalid parameters.");
				}
			}
			toDateTruncated = DateUtils.truncate((Date)toDate, Calendar.DATE);
			if(fromDateTruncated.after(toDateTruncated))
			{
				continue;
			}
			int fromDateYear = DateUtils.toCalendar(fromDateTruncated).get(Calendar.YEAR);
			int toDateYear = DateUtils.toCalendar(toDateTruncated).get(Calendar.YEAR);
			if(fromDateYear == toDateYear)
			{
				dateRangeMap.put("from", fromDateTruncated);
				dateRangeMap.put("to", toDateTruncated);
				dateList.add(dateRangeMap);
			}
			else
			{
				for(int j = fromDateYear; j <= toDateYear; j++)
				{
					if(j == fromDateYear)
					{
						Map<String, Date> dateRangeYearSplitMap = new HashMap<String, Date>();
						Date dateBegOfYear = fromDateTruncated;
						dateRangeYearSplitMap.put("from", dateBegOfYear);
						Calendar calEndOfYear = DateUtils.toCalendar(new Date());
						calEndOfYear.set(j, Calendar.DECEMBER, 30, 24, 60, 60);
						Date dateEndOfNextYear = DateUtils.truncate(calEndOfYear.getTime(), Calendar.DATE);
						dateRangeYearSplitMap.put("to", dateEndOfNextYear);
						dateList.add(dateRangeYearSplitMap);
					}
					else if(j == toDateYear)
					{
						Map<String, Date> dateRangeYearSplitMap = new HashMap<String, Date>();
						Calendar calBegOfYear = DateUtils.toCalendar(new Date());
						calBegOfYear.set(j, Calendar.JANUARY, 0, 24, 60, 60);
						Date dateBegOfYear = DateUtils.truncate(calBegOfYear.getTime(), Calendar.DATE);
						dateRangeYearSplitMap.put("from", dateBegOfYear);
						Date dateEndOfNextYear = toDateTruncated;
						dateRangeYearSplitMap.put("to", dateEndOfNextYear);
						dateList.add(dateRangeYearSplitMap);
					}
					else
					{
						Map<String, Date> dateRangeYearSplitMap = new HashMap<String, Date>();
						Calendar calBegOfYear = DateUtils.toCalendar(new Date());
						calBegOfYear.set(j, Calendar.JANUARY, 0, 24, 60, 60);
						Date dateBegOfYear = DateUtils.truncate(calBegOfYear.getTime(), Calendar.DATE);
						dateRangeYearSplitMap.put("from", dateBegOfYear);
						Calendar calEndOfYear = DateUtils.toCalendar(new Date());
						calEndOfYear.set(j, Calendar.DECEMBER, 30, 24, 60, 60);
						Date dateEndOfNextYear = DateUtils.truncate(calEndOfYear.getTime(), Calendar.DATE);
						dateRangeYearSplitMap.put("to", dateEndOfNextYear);
						dateList.add(dateRangeYearSplitMap);
					}
				}
			}
		}
		if(anchorStart != null)
		{
			Map<String, Date> anchorStartMap = new HashMap<String, Date>();
			anchorStartMap.put("from", anchorStart);
			anchorStartMap.put("to", anchorStart);
			dateList.add(anchorStartMap);
		}
		if(anchorEnd != null)
		{
			Map<String, Date> anchorEndMap = new HashMap<String, Date>();
			anchorEndMap.put("from", anchorEnd);
			anchorEndMap.put("to", anchorEnd);
			dateList.add(anchorEndMap);
		}
		List<Map<String, Date>> dateChildList = new ArrayList<Map<String, Date>>();
		dateChildList.addAll(dateList);
		Iterator<Map<String, Date>> dateIt = dateList.iterator();
		int counter = 0;
		while(dateIt.hasNext())
		{
			Map<String, Date> dateMap = dateIt.next();
			Date fromDate = dateMap.get("from");
			Date toDate = dateMap.get("to");
			if(fromDate == null && toDate == null)
			{
				counter++;
				continue;
			}
			Iterator<Map<String, Date>> dateChildIt = dateChildList.iterator();
			int childCounter = 0;
			while(dateChildIt.hasNext())
			{
				Map<String, Date> dateChildMap = dateChildIt.next();
				if(childCounter == counter)
				{
					childCounter++;
					continue;
				}
				Date fromChildDate = dateChildMap.get("from");
				Date toChildDate = dateChildMap.get("to");
				if(fromChildDate == null && toChildDate == null)
				{
					childCounter++;
					continue;
				}
				if((fromDate.after(fromChildDate) || fromDate.equals(fromChildDate)))
				{
					if(fromDate.before(toChildDate) || fromDate.equals(toChildDate))
					{
						if(toDate.before(toChildDate) || toDate.equals(toChildDate))
						{
							dateMap.remove("from");
							dateMap.remove("to");
							break;
						}
						else
						{
							dateMap.remove("from");
							dateMap.remove("to");
							dateChildMap.put("to", toDate);
							break;
						}
					}
				}
				else
				{
					if(toDate.after(toChildDate) || toDate.equals(toChildDate))
					{
						dateChildMap.remove("from");
						dateChildMap.remove("to");
					}
					else if(toDate.after(fromChildDate) || toDate.equals(fromChildDate))
					{
						dateMap.remove("from");
						dateMap.remove("to");
						dateChildMap.put("from", fromDate);
						break;
					}
				}
				childCounter++;
			}
			counter++;
		}
		return dateList;
	}
}
