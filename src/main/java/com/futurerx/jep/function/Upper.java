/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class Upper extends PostfixMathCommand
{
	public Upper()
	{
		numberOfParameters = 1;
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		checkStack(inStack); // check the stack
		Object param1 = inStack.pop();
		String retVal = null;
		if(param1 == null)
		{
			retVal = null;
		}
		if(param1 instanceof String)
		{
			retVal = ((String)param1).toUpperCase();
		}
		else
		{
			throw new ParseException("Invalid parameters.");
		}
		inStack.push(retVal);
		return;
	}
}
