/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

 *****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;

import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * The list function. Returns a Vector comprising all the children.
 * 
 * @author Rich Morris Created on 29-Feb-2004
 */
public class Max extends PostfixMathCommand {
	public Max() {
		numberOfParameters = -1;
	}

	public void run(Stack inStack) throws ParseException {
		int numParams = this.curNumberOfParameters;
		if (numParams < 1) {
			throw new ParseException(
					"'min' operator must have more than 0 arguments.");
		}
		checkStack(inStack); // check the stack
		List<Object> inList = new ArrayList<Object>();
		for (int i = 0; i < numParams; i++) {
			Object param2 = inStack.pop();
			if (param2 == null) {
				continue;
			}
			if (param2 instanceof Collection) {
				inList.addAll((Collection) param2);
				continue;
			} else {
				inList.add(param2);
			}
		}
		Object retVal = null;
		String prevDataType = null;
		for(int i = 0; i < inList.size(); i++)
		{
			Object inElement = inList.get(i);
			if((!(inElement instanceof Number)) && (!(inElement instanceof Date)))
			{
				throw new ParseException(
				"'max' operator must have Numbers or Dates as parameters.");
			}
			String dataType = null;
			if(inElement instanceof Number)
			{
				dataType = "NUMBER";
			}
			else
			{
				dataType = "DATE";
			}
			if(prevDataType != null && (!dataType.equals(prevDataType)))
			{
				throw new ParseException(
				"'min' operator must have same type of parameters.");
			}
			else
			{
				prevDataType = dataType;
			}
			if(dataType.equals("NUMBER"))
			{
				if(i == 0)
				{
					retVal = Double.MIN_VALUE;
				}
				if(((Number)inElement).doubleValue() > ((Number)retVal).doubleValue())
				{
					retVal = inElement;
				}
			}
			else
			{
				if(i == 0)
				{
					GregorianCalendar param1Cal = new GregorianCalendar();
					param1Cal.setTime(new Date());
					param1Cal.add(Calendar.YEAR, -1000);
					retVal = param1Cal.getTime();				
				}
				if(((Date)inElement).after(((Date)retVal)))
				{
					retVal = inElement;
				}
			}
		}
		inStack.push(retVal);
		return;
	}
}
