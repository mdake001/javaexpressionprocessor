/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

 *****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * The list function. Returns a Vector comprising all the children.
 * 
 * @author Rich Morris Created on 29-Feb-2004
 */
public class Matches extends PostfixMathCommand {
	public Matches() {
		numberOfParameters = -1;
	}

	public void run(Stack inStack) throws ParseException {
		int numParams = this.curNumberOfParameters;
		if (numParams < 2) {
			throw new ParseException(
					"'matches' operator must have more than 1 arguments.");
		}
		checkStack(inStack); // check the stack
		List<Object> inList = new ArrayList<Object>();
		for (int i = 0; i < numParams - 1; i++) {
			Object param2 = inStack.pop();
			if (param2 == null) {
				continue;
			}
			if (param2 instanceof Collection) {
				inList.addAll((Collection) param2);
				continue;
			} else {
				inList.add(param2);
			}
		}
		Object param1 = inStack.pop();
		boolean retVal = false;
		if (param1 == null) {
			retVal = false;
		}
		else
		{
			for(int i = 0; i < inList.size(); i++)
			{
				Object inElement = inList.get(i);
				if(!(inElement instanceof String))
				{
					throw new ParseException(
					"'avg' operator must have Numbers as parameters.");
				}
				retVal = ((String) param1).matches((String) inElement);
				if(retVal)
				{
					break;
				}
			}
		}
		inStack.push(retVal);
		return;
	}
	
	/*
	public void run(Stack inStack) throws ParseException {
		checkStack(inStack); // check the stack
		Object param2 = inStack.pop();
		Object param1 = inStack.pop();
		boolean retVal = false;
		if (param1 == null && param2 == null) {
			retVal = true;
		} else if (param1 == null || param2 == null) {
			retVal = false;
		} else if (param1 instanceof String && param2 instanceof String) {
			try
			{
				retVal = ((String) param1).matches((String) param2);
			}
			catch(Exception e)
			{
				System.out.println("ERR IN MATCH");
				e.printStackTrace();
			}
		} else {
			throw new ParseException(
					"'matches' operator must have String arguments.");
		}
		inStack.push(retVal);
		return;
	}
	*/
}
