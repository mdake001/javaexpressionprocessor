/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathContextFactory;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

import com.futurerx.jep.Context;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class GetData extends PostfixMathCommand
{
	Context context = null;
	
	public GetData(Context context)
	{
		numberOfParameters = -1;
		this.context = context;
	}
	
	public GetData()
	{
		numberOfParameters = -1;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		int numParams = this.curNumberOfParameters;
		if (numParams > 2) {
			throw new ParseException(
					"'getData' operator must have 1 or 2 arguments.");
		}
		checkStack(inStack); // check the stack
		Object param1 = inStack.pop();
		Object retVal = null;
		if(param1 == null)
		{
			throw new ParseException("Invalid parameters.");
		}
		else if(!(param1 instanceof String))
		{
			throw new ParseException("Invalid parameters.");
		}
		Object param2 = null;
		if (numParams == 2) {
			param2 = inStack.pop();
			if(param2 == null)
			{
				throw new ParseException("Invalid parameters.");
			}
			else if(!(param2 instanceof String))
			{
				throw new ParseException("Invalid parameters.");
			}
		}
		String dataNode = null;
		String relativeDataNode = null;
		if(numParams == 1)
		{
			dataNode = (String)param1;
		}
		else
		{
			dataNode = (String)param2;
			relativeDataNode = (String)param1;
		}
		//JXPathContext jxPathcontext = JXPathContextFactory.newInstance().newContext(null, this.context.getObject());
		JXPathContext jxPathcontext = JXPathContext.newContext(this.context.getObject());
		jxPathcontext.setLenient(true);
//		((HashMap)((HashMap)((HashMap)this.context.getObject()).get("code")).get("CPT")).get("58140")
		retVal = jxPathcontext.getValue(dataNode);
		if(relativeDataNode != null)
		{
			if(retVal instanceof Collection)
			{
				List<Object> colRetVal = new ArrayList<Object>();
				Iterator colIt = ((Collection)retVal).iterator();
				while(colIt.hasNext())
				{
					Object colVal = colIt.next();
					JXPathContext context = JXPathContext.newContext(colVal);
					colRetVal.add(context.getValue(relativeDataNode));
				}
				retVal = colRetVal;
			}
			else
			{
				JXPathContext context = JXPathContext.newContext(retVal);
				retVal = context.getValue(relativeDataNode);
			}
		}
		inStack.push(retVal);
		return;
	}
}
