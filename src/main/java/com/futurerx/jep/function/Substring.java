/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class Substring extends PostfixMathCommand
{
	public Substring()
	{
		numberOfParameters = -1;
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		checkStack(inStack); // check the stack
		if(this.curNumberOfParameters > 3 || this.curNumberOfParameters < 2)
		{
			throw new ParseException("Invalid parameters.");
		}
		
		Object param1 = null;
		Object param2 = null;
		Object param3 = null;
		if(this.curNumberOfParameters == 2)
		{
			param2 = inStack.pop();
			param1 = inStack.pop();
			if((!(param1 instanceof String && param2 instanceof Number)) || (param1 == null || param2 == null))
			{
				throw new ParseException("Invalid parameters.");
			}
		}
		else
		{
			param3 = inStack.pop();
			param2 = inStack.pop();
			param1 = inStack.pop();
			if((!(param1 instanceof String && param2 instanceof Number && param3 instanceof Number)) || (param1 == null || param2 == null || param3 == null))
			{
				throw new ParseException("Invalid parameters.");
			}
		}
		String retVal = null;
		if(param1 == null)
		{
			retVal = null;
		}
		else
		{
			if(param3 == null)
			{
				retVal = ((String)param1).substring(((Number)param2).intValue());
			}
			else
			{
				retVal = ((String)param1).substring(((Number)param2).intValue(), ((Number)param3).intValue());
			}
		}
		inStack.push(retVal);
		return;
	}
}
