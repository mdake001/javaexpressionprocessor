/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class LPad extends PostfixMathCommand
{
	public LPad()
	{
		numberOfParameters = 3;
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		checkStack(inStack); // check the stack
		
		Object param1 = null;
		Object param2 = null;
		Object param3 = null;
	
		param3 = inStack.pop();
		param2 = inStack.pop();
		param1 = inStack.pop();
		if((!(param1 instanceof String && param2 instanceof Number && param3 instanceof String)) || (param1 == null || param2 == null || param3 == null))
		{
			throw new ParseException("Invalid parameters.");
		}
		
		String retVal = null;
		
		retVal = String.format("%"+((Double)param2).intValue()+"s", (String)param1).replace(" ", (String)param3);
		inStack.push(retVal);
		return;
	}
}
