/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

 *****************************************************************************/

package com.futurerx.jep.function;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * The list function. Returns a Vector comprising all the children.
 * 
 * @author Rich Morris Created on 29-Feb-2004
 */
public class GetSQLData extends PostfixMathCommand {
	Connection connection = null;

	public GetSQLData(Connection connection) {
		this.connection = connection;
		numberOfParameters = -1;
	}

	public void run(Stack inStack) throws ParseException {
		int numParams = this.curNumberOfParameters;
		if (numParams < 2) {
			throw new ParseException(
					"'GetSQLData' operator must have at least 1 arguments.");
		}
		checkStack(inStack); // check the stack
		List<Object> inList = new ArrayList<Object>();
		for (int i = 0; i < numParams - 1; i++) {
			Object param2 = inStack.pop();
			if (param2 == null) {
				continue;
			}
			if (param2 instanceof Collection) {
				inList.addAll((Collection) param2);
				continue;
			} else {
				inList.add(param2);
			}
		}
		Object param1 = inStack.pop();
		if (param1 == null || !(param1 instanceof String)) {
			throw new ParseException("Invalid parameters.");
		}
		List<Map<String, Object>> retVal = new ArrayList<Map<String, Object>>();
		try {

			// Get a statement from the connection
			PreparedStatement stmt = this.connection.prepareStatement((String)param1);

			// Execute the query
//			System.out.println("SQL:" + param1);
			int paramIndex = 1;
			for(int i = inList.size() -  1; i >=0; i--)
			{
				stmt.setObject(paramIndex, inList.get(i));
				paramIndex++;
			}

			// Get the metadata
			ResultSet rs = stmt.executeQuery();
			ResultSetMetaData md = rs.getMetaData();

			// Print the column labels
//			for (int i = 1; i <= md.getColumnCount(); i++)
//				System.out.print(md.getColumnLabel(i) + " ");
//			System.out.println();

			// Loop through the result set
			while (rs.next()) {
				Map<String, Object> data = new HashMap<String, Object>();
				for (int i = 1; i <= md.getColumnCount(); i++)
				{
					data.put(md.getColumnName(i), rs.getObject(i));
				}
				retVal.add(data);
			}

			// Close the result set, statement and the connection
			rs.close();
			stmt.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw new ParseException("SQL Exception.");
		}
		inStack.push(retVal);
		return;
	}
}
