/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

 *****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * The list function. Returns a Vector comprising all the children.
 * 
 * @author Rich Morris Created on 29-Feb-2004
 */
public class In extends PostfixMathCommand {
	public In() {
		numberOfParameters = -1;
	}

	public void run(Stack inStack) throws ParseException {
		int numParams = this.curNumberOfParameters;
		if (numParams < 2) {
			throw new ParseException(
					"'in' operator must have more than 1 arguments.");
		}
		checkStack(inStack); // check the stack
		List<Object> inList = new ArrayList<Object>();
		for (int i = 0; i < numParams - 1; i++) {
			Object param2 = inStack.pop();
			if (param2 == null) {
				continue;
			}
			if (param2 instanceof Collection) {
				inList.addAll((Collection) param2);
				continue;
			} else {
				inList.add(param2);
			}
		}
		Object param1 = inStack.pop();
		boolean retVal = false;
		if (param1 == null) {
			retVal = false;
		}
		else
		{
			if (inList.contains(param1)) {
				retVal = true;
			}
		}
		inStack.push(retVal);
		return;
	}
}
