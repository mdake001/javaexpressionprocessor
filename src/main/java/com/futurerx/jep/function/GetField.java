/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
 April 30 2007
 (c) Copyright 2007, Nathan Funk and Richard Morris
 See LICENSE-*.txt for license information.

 *****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;
import org.nfunk.jep.type.*;

import com.futurerx.jep.Util;

/**
 * Addition function. Supports any number of parameters although typically only
 * 2 parameters are used.
 * 
 * @author nathan
 */
public class GetField extends PostfixMathCommand
{

	public GetField()
	{
		numberOfParameters = 2;
	}

	/**
	 * Calculates the result of applying the "+" operator to the arguments from
	 * the stack and pushes it back on the stack.
	 */
	public void run(Stack stack) throws ParseException
	{
		checkStack(stack);// check the stack

		try
		{
			ClassLoader loader = this.getClass()
					.getClassLoader();

			String fieldName = (String) stack.pop();
			String className = "";
			Object classInstance = stack.pop();
			if(className.equals(""))
			{
				className = classInstance.getClass().getName();
			}
			Class clazz = null;
			clazz = loader.loadClass(className);
			java.lang.reflect.Field refField = Util.getClassField(
					clazz, fieldName);
			Object retVal = Util.getClassFieldValue(refField,
					classInstance);
			stack.push(retVal);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ParseException("Unable to load Class");
		}
		return;
	}
}
