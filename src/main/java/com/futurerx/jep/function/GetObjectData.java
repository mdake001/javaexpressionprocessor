/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathContextFactory;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

import com.futurerx.jep.Context;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class GetObjectData extends PostfixMathCommand
{
	Context context = null;
	
	public GetObjectData()
	{
		numberOfParameters = -1;
	}

	public void run(Stack inStack)
		throws ParseException 
	{
		int numParams = this.curNumberOfParameters;
		if (numParams > 2) {
			throw new ParseException(
					"'getObjectData' operator must have 1 or 2 arguments.");
		}
		checkStack(inStack); // check the stack
		Object param1 = inStack.pop();
		Object retVal = null;
		if(param1 == null)
		{
			throw new ParseException("Invalid parameters.");
		}
		else if(!(param1 instanceof String))
		{
			throw new ParseException("Invalid parameters.");
		}
		Object param2 = inStack.pop();
		if(param2 == null)
		{
			throw new ParseException("Invalid parameters.");
		}
		JXPathContext jxPathcontext = JXPathContext.newContext(param2);
		jxPathcontext.setLenient(true);
		retVal = jxPathcontext.getValue((String)param1);
		inStack.push(retVal);
		return;
	}
}
