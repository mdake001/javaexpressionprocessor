/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

 *****************************************************************************/

package com.futurerx.jep.function;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * The list function. Returns a Vector comprising all the children.
 * 
 * @author Rich Morris Created on 29-Feb-2004
 */
public class ExecuteSQL extends PostfixMathCommand {
	Connection connection = null;

	public ExecuteSQL(Connection connection) {
		this.connection = connection;
		numberOfParameters = -1;
	}

	public void run(Stack inStack) throws ParseException {
		int numParams = this.curNumberOfParameters;
		if (numParams != 1) {
			throw new ParseException(
					"'ExecuteSQL' operator must have at only 1 argument.");
		}
		checkStack(inStack); // check the stack
		List<Object> inList = new ArrayList<Object>();
		for (int i = 0; i < numParams - 1; i++) {
			Object param2 = inStack.pop();
			if (param2 == null) {
				continue;
			}
			if (param2 instanceof Collection) {
				inList.addAll((Collection) param2);
				continue;
			} else {
				inList.add(param2);
			}
		}
		Object param1 = inStack.pop();
		if (param1 == null || !(param1 instanceof String)) {
			throw new ParseException("Invalid parameters.");
		}
		boolean retVal = false;
		try {

			// Get a statement from the connection
			PreparedStatement stmt = this.connection.prepareStatement((String)param1);

			// Execute the query
//			System.out.println("SQL:" + param1);
			int paramIndex = 1;
			for(int i = inList.size() -  1; i >=0; i--)
			{
				stmt.setObject(paramIndex, inList.get(i));
				paramIndex++;
			}

			// Get the metadata
			retVal = stmt.execute();
			stmt.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw new ParseException("SQL Exception.");
		}
		inStack.push(retVal);
		return;
	}
}
