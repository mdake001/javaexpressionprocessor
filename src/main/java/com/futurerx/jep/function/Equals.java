/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class Equals extends PostfixMathCommand
{
	public Equals()
	{
		numberOfParameters = 2;
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		checkStack(inStack); // check the stack
		Object param2 = inStack.pop();
		Object param1 = inStack.pop();
		boolean retVal = false;
		if(param1 == null && param2 == null)
		{
			retVal = true;
		}
		else if(param1 == null || param2 == null)
		{
			retVal = false;
		}
		else if(param1 instanceof Number && param2 instanceof Number)
		{
			retVal = (((Number)param1).doubleValue() == ((Number)param2).doubleValue());
		}
		else if(param1 instanceof String && param2 instanceof String)
		{
			retVal = param1.equals(param2);
		}
		else if(param1 instanceof Date && param2 instanceof Date)
		{
			GregorianCalendar param1Cal = new GregorianCalendar();
			param1Cal.setTime((Date)param1);
			GregorianCalendar param2Cal = new GregorianCalendar();
			param2Cal.setTime((Date)param2);
			retVal = param1Cal.equals(param2Cal);
		}
		else
		{
			throw new ParseException("Invalid parameters.");
		}
		inStack.push(retVal);
		return;
	}
}
