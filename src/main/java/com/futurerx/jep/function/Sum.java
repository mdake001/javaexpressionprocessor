/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

 *****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * The list function. Returns a Vector comprising all the children.
 * 
 * @author Rich Morris Created on 29-Feb-2004
 */
public class Sum extends PostfixMathCommand {
	public Sum() {
		numberOfParameters = -1;
	}

	public void run(Stack inStack) throws ParseException {
		int numParams = this.curNumberOfParameters;
		if (numParams < 1) {
			throw new ParseException(
					"'sum' operator must have more than 0 arguments.");
		}
		checkStack(inStack); // check the stack
		List<Object> inList = new ArrayList<Object>();
		for (int i = 0; i < numParams; i++) {
			Object param2 = inStack.pop();
			if (param2 == null) {
				continue;
			}
			if (param2 instanceof Collection) {
				inList.addAll((Collection) param2);
				continue;
			} else {
				inList.add(param2);
			}
		}
		double retVal = 0;
		for(int i = 0; i < inList.size(); i++)
		{
			Object inElement = inList.get(i);
			if(!(inElement instanceof Number))
			{
				throw new ParseException(
				"'sum' operator must have Numbers as parameters.");
			}
			retVal += ((Number)inElement).doubleValue();
		}
		inStack.push(new Double(retVal));
		return;
	}
}
