/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import org.apache.commons.jxpath.JXPathContext;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;

import com.futurerx.jep.Context;
import com.futurerx.jep.JxpathObjectFactory;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class SetData extends PostfixMathCommand
{
	Context context = null;
	
	public SetData(Context context)
	{
		numberOfParameters = -1;
		this.context = context;
	}

	public SetData()
	{
		numberOfParameters = -1;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		int numParams = this.curNumberOfParameters;
		if (numParams > 2) {
			throw new ParseException(
					"'getData' operator must have 2 arguments.");
		}
		checkStack(inStack); // check the stack
		Object param2 = null;
		if (numParams == 2) {
			param2 = inStack.pop();
//			if(param2 == null)
//			{
//				throw new ParseException("Invalid parameters.");
//			}
		}
		Object param1 = inStack.pop();
		Object retVal = null;
		if(param1 == null)
		{
			throw new ParseException("Invalid parameters.");
		}
		else if(!(param1 instanceof String))
		{
			throw new ParseException("Invalid parameters.");
		}
		
		Object dataNode = null;
		String relativeDataNode = null;
		if(numParams == 1)
		{
			dataNode = (String)param1;
		}
		else
		{
			dataNode = param2;
			relativeDataNode = (String)param1;
		}
		//JXPathContext jxPathcontext = JXPathContextFactory.newInstance().newContext(null, this.context.getObject());
		JXPathContext jxPathcontext = JXPathContext.newContext(this.context.getObject());
		jxPathcontext.setFactory(new JxpathObjectFactory());
		jxPathcontext.setLenient(true);
		jxPathcontext.createPathAndSetValue(param1.toString(), param2);
		inStack.push(true);
		return;
	}
}
