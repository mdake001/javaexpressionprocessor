/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

 *****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * The list function. Returns a Vector comprising all the children.
 * 
 * @author Rich Morris Created on 29-Feb-2004
 */
public class MatchesCodeSet extends PostfixMathCommand {
	public MatchesCodeSet() {
		numberOfParameters = -1;
	}

	public void run(Stack inStack) throws ParseException {
		int numParams = this.curNumberOfParameters;
		if (numParams < 3) {
			throw new ParseException(
					"'matches' operator must have more than 1 arguments.");
		}
		checkStack(inStack); // check the stack
		List<Object> inList = new ArrayList<Object>();
		for (int i = 0; i < numParams - 2; i++) {
			Object param2 = inStack.pop();
			if (param2 == null) {
				continue;
			}
			if (param2 instanceof Collection) {
				inList.addAll((Collection) param2);
				continue;
			} else {
				inList.add(param2);
			}
		}
		Object param2 = inStack.pop();
		Object param1 = inStack.pop();
		if(!(param2 instanceof Number))
		{
			throw new ParseException(
			"'avg' operator must have Numbers as parameters.");
		}
		int intParam2 = ((Number)param2).intValue();
		boolean retVal = false;
		if (param1 == null) {
			retVal = false;
		}
		else
		{
			for(int i = 0; i < inList.size(); i++)
			{
				Object inElement = inList.get(i);
				if(!(inElement instanceof String))
				{
					throw new ParseException(
					"'avg' operator must have Numbers as parameters.");
				}
				String strInElement = (String)inElement;
				int indexOfDash = strInElement.indexOf("-");
				if(indexOfDash == -1)
				{
					retVal = ((String) param1).matches(this.getCodeRegex(strInElement, intParam2));
				}
				else
				{
					String fromStr = strInElement.substring(0, indexOfDash);
					String toStr = strInElement.substring(indexOfDash + 1);
					boolean valLessThanEqToTo = ((String)param1).compareTo((String)toStr) <= 0;
					boolean valGreaterThanEqToFrom = ((String)param1).compareTo((String)fromStr) >= 0;
					//System.out.println(param1 + ":" + fromStr + ":" + toStr + ":" + valLessThanEqToTo + ":" + valGreaterThanEqToFrom);
					retVal = valLessThanEqToTo && valGreaterThanEqToFrom;
				}
				if(retVal)
				{
					break;
				}
			}
		}
		inStack.push(retVal);
		return;
	}

	private String getCodeRegex(String code, int length)
	{
		String retVal = null;
		boolean isPartial = (length > code.length());
		retVal = code.replaceAll("x", ".{1}");
		if(isPartial)
		{
			retVal = retVal + ".*";
		}
		//System.out.println("getCodeRegex:" + retVal);
		return retVal;
	}
}
