/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

 *****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * The list function. Returns a Vector comprising all the children.
 * 
 * @author Rich Morris Created on 29-Feb-2004
 */
public class Concat extends PostfixMathCommand {
	public Concat() {
		numberOfParameters = -1;
	}

	public void run(Stack inStack) throws ParseException {
		int numParams = this.curNumberOfParameters;
		checkStack(inStack); // check the stack
		List<String> params = new ArrayList<String>();
		for (int i = 0; i < numParams; i++) {
			Object param1 = inStack.pop();
			if(param1 == null)
			{
				continue;
			}
			if(!(param1 instanceof String))
			{
				throw new ParseException("Invalid parameters.");
			}
			params.add((String)param1);
		}
		StringBuffer buff = new StringBuffer();
		for(int i = params.size() - 1; i >= 0; i--)
		{
			buff.append(params.get(i));
		}
		inStack.push(buff.toString());
		return;
	}
}
