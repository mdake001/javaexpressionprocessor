/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function.temp;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class NotEndsWith extends PostfixMathCommand
{
	public NotEndsWith()
	{
		numberOfParameters = 2;
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		checkStack(inStack); // check the stack
		Object param2 = inStack.pop();
		Object param1 = inStack.pop();
		boolean retVal = false;
		if(param1 == null || param2 == null)
		{
			retVal = false;
		}
		if(!(param1 instanceof String) || !(param2 instanceof String))
		{
			retVal = false;
		}
		else
		{
			retVal = ((String)param1).endsWith(((String)param2));
		}
		inStack.push(!retVal);
		return;
	}
}
