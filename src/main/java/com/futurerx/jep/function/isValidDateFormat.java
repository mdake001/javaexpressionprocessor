package com.futurerx.jep.function;

import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

public class isValidDateFormat extends PostfixMathCommand {

	public isValidDateFormat()
	{
		numberOfParameters = 1;
	}
	
	public void run(Stack inStack)
			throws ParseException 
		{
			checkStack(inStack);
			Object param1 = inStack.pop();
			boolean retVal = false;
			if(param1 == null) {
				retVal = false;
			} else {
				retVal = (new SimpleDateFormat("MM/dd/yyyy").format(param1).equals("01/01/0002"));
			}
			inStack.push(retVal);
			return;
		}
}