/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class AddDatePart extends PostfixMathCommand
{
	private Map<String, Integer> dateUnitMap = new HashMap<String, Integer>();
	public AddDatePart()
	{
		numberOfParameters = 3;
		dateUnitMap.put("ERA", Calendar.ERA);
		dateUnitMap.put("YEAR", Calendar.YEAR);
		dateUnitMap.put("MONTH", Calendar.MONTH);
		dateUnitMap.put("WEEK_OF_YEAR", Calendar.WEEK_OF_YEAR);
		dateUnitMap.put("WEEK_OF_MONTH", Calendar.WEEK_OF_MONTH);
		dateUnitMap.put("DATE", Calendar.DATE);
		dateUnitMap.put("DAY_OF_MONTH", Calendar.DAY_OF_MONTH);
		dateUnitMap.put("DAY_OF_YEAR", Calendar.DAY_OF_YEAR);
		dateUnitMap.put("DAY_OF_WEEK", Calendar.DAY_OF_WEEK);
		dateUnitMap.put("DAY_OF_WEEK_IN_MONTH", Calendar.DAY_OF_WEEK_IN_MONTH);
		dateUnitMap.put("AM_PM", Calendar.AM_PM);
		dateUnitMap.put("HOUR", Calendar.HOUR);
		dateUnitMap.put("HOUR_OF_DAY", Calendar.HOUR_OF_DAY);
		dateUnitMap.put("MINUTE", Calendar.MINUTE);
		dateUnitMap.put("SECOND", Calendar.SECOND);
		dateUnitMap.put("MILLISECOND", Calendar.MILLISECOND);
		dateUnitMap.put("ZONE_OFFSET", Calendar.ZONE_OFFSET);
		dateUnitMap.put("DST_OFFSET", Calendar.DST_OFFSET);
		dateUnitMap.put("SUNDAY", Calendar.SUNDAY);
		dateUnitMap.put("MONDAY", Calendar.MONDAY);
		dateUnitMap.put("TUESDAY", Calendar.TUESDAY);
		dateUnitMap.put("WEDNESDAY", Calendar.WEDNESDAY);
		dateUnitMap.put("THURSDAY", Calendar.THURSDAY);
		dateUnitMap.put("FRIDAY", Calendar.FRIDAY);
		dateUnitMap.put("SATURDAY", Calendar.SATURDAY);
		dateUnitMap.put("JANUARY", Calendar.JANUARY);
		dateUnitMap.put("FEBRUARY", Calendar.FEBRUARY);
		dateUnitMap.put("MARCH", Calendar.MARCH);
		dateUnitMap.put("APRIL", Calendar.APRIL);
		dateUnitMap.put("MAY", Calendar.MAY);
		dateUnitMap.put("JUNE", Calendar.JUNE);
		dateUnitMap.put("JULY", Calendar.JULY);
		dateUnitMap.put("AUGUST", Calendar.AUGUST);
		dateUnitMap.put("SEPTEMBER", Calendar.SEPTEMBER);
		dateUnitMap.put("OCTOBER", Calendar.OCTOBER);
		dateUnitMap.put("NOVEMBER", Calendar.NOVEMBER);
		dateUnitMap.put("DECEMBER", Calendar.DECEMBER);
		dateUnitMap.put("AM", Calendar.AM);
		dateUnitMap.put("PM", Calendar.PM);
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		checkStack(inStack); // check the stack
		Object param3 = inStack.pop();
		Object param2 = inStack.pop();
		Object param1 = inStack.pop();
		Date retVal = null;
		
        if(param1 == null || param2 == null)
		{
			retVal = null;
		}
		else if(param1 instanceof Date && param2 instanceof String && (param3 instanceof Number || param3 instanceof String))
		{
			int param2Int = 0;
			int param3Int = 0;
			if(param3 instanceof String)
			{
				if(this.dateUnitMap.containsKey(param3))
				{
					param3Int = this.dateUnitMap.get(param3);
				}
				else
				{
					throw new ParseException("Invalid parameters.");
				}
			}
			else
			{
				param3Int = ((Number)param3).intValue();
			}
			if(this.dateUnitMap.containsKey(param2))
			{
				param2Int = this.dateUnitMap.get(param2);
			}
			else
			{
				throw new ParseException("Invalid parameters.");
			}
			GregorianCalendar param1Cal = new GregorianCalendar();
			param1Cal.setTime((Date)param1);
			param1Cal.add(param2Int, param3Int);
			retVal = param1Cal.getTime();
		}
		else
		{
			throw new ParseException("Invalid parameters.");
		}
		inStack.push(retVal);
		return;
	}
}
