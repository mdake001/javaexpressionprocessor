/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import org.apache.commons.jxpath.JXPathContext;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;

import com.futurerx.jep.Context;
import com.futurerx.jep.JxpathObjectFactory;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class SetObjectData extends PostfixMathCommand
{
	Context context = null;
	
	public SetObjectData()
	{
		numberOfParameters = -1;
	}

	public void run(Stack inStack)
		throws ParseException 
	{
		int numParams = this.curNumberOfParameters;
		if (numParams > 3) {
			throw new ParseException(
					"'setObjectData' operator must have 3 arguments.");
		}
		checkStack(inStack); // check the stack
		Object param3 = inStack.pop();
		Object param2 = inStack.pop();
		Object param1 = inStack.pop();
		Object retVal = null;
		if(param2 == null)
		{
			throw new ParseException("Invalid parameters.");
		}
		else if(!(param2 instanceof String))
		{
			throw new ParseException("Invalid parameters.");
		}
		if(param1 == null)
		{
			throw new ParseException("Invalid parameters.");
		}
		
		JXPathContext jxPathcontext = JXPathContext.newContext(param1);
		jxPathcontext.setFactory(new JxpathObjectFactory());
		jxPathcontext.setLenient(true);
		jxPathcontext.createPathAndSetValue(param2.toString(), param3);
		inStack.push(true);
		return;
	}
}
