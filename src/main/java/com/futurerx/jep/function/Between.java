/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class Between extends PostfixMathCommand
{
	public Between()
	{
		numberOfParameters = 3;
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		checkStack(inStack); // check the stack
		Object param3 = inStack.pop();
		Object param2 = inStack.pop();
		Object param1 = inStack.pop();
		boolean retValGreaterOrEqual = false;
		if(param1 == null && param2 == null)
		{
			retValGreaterOrEqual = false;
		}
		else if(param1 == null)
		{
			retValGreaterOrEqual = true;
		}
		else if(param2 == null)
		{
			retValGreaterOrEqual = false;
		}
		else if(param1 instanceof Number && param2 instanceof Number)
		{
			retValGreaterOrEqual = (((Number)param1).doubleValue() < ((Number)param2).doubleValue());
		}
		else if(param1 instanceof String && param2 instanceof String)
		{
			retValGreaterOrEqual = ((String)param1).compareTo((String)param2) < 0;
		}
		else
		{
			throw new ParseException("Invalid parameters.");
		}
		retValGreaterOrEqual = !retValGreaterOrEqual;
		
		
		boolean retValLessOrEqual = false;
		if(param1 == null && param3 == null)
		{
			retValLessOrEqual = false;
		}
		else if(param1 == null)
		{
			retValLessOrEqual = false;
		}
		else if(param3 == null)
		{
			retValLessOrEqual = true;
		}
		else if(param1 instanceof Number && param3 instanceof Number)
		{
			retValLessOrEqual = (((Number)param1).doubleValue() > ((Number)param3).doubleValue());
		}
		else if(param1 instanceof String && param3 instanceof String)
		{
			retValLessOrEqual = ((String)param1).compareTo((String)param3) > 0;
		}
		else
		{
			throw new ParseException("Invalid parameters.");
		}
		retValLessOrEqual = !retValLessOrEqual;
		
		inStack.push(retValGreaterOrEqual && retValLessOrEqual);
		return;
	}
}
