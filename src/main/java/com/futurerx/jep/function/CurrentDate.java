/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
      April 30 2007
      (c) Copyright 2007, Nathan Funk and Richard Morris
      See LICENSE-*.txt for license information.

*****************************************************************************/

package com.futurerx.jep.function;

import java.text.SimpleDateFormat;
import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;

/** The list function.
 * Returns a Vector comprising all the children.
 * 
 * @author Rich Morris
 * Created on 29-Feb-2004
 */
public class CurrentDate extends PostfixMathCommand
{
	private Map<String, Integer> dateUnitMap = new HashMap<String, Integer>();
	public CurrentDate()
	{
		numberOfParameters = 0;
	}
	
	public void run(Stack inStack)
		throws ParseException 
	{
		checkStack(inStack); // check the stack
		Date retVal = new Date();
		inStack.push(retVal);
		return;
	}
}
