/*****************************************************************************

 JEP 2.4.1, Extensions 1.1.1
 April 30 2007
 (c) Copyright 2007, Nathan Funk and Richard Morris
 See LICENSE-*.txt for license information.

 *****************************************************************************/

package com.futurerx.jep.function;

import java.util.*;
import org.nfunk.jep.*;
import org.nfunk.jep.function.PostfixMathCommand;
import org.nfunk.jep.type.*;

import com.futurerx.jep.Util;

/**
 * Addition function. Supports any number of parameters although typically only
 * 2 parameters are used.
 * 
 * @author nathan
 */
public class Invoke extends PostfixMathCommand
{

	public Invoke()
	{
		numberOfParameters = -1;
	}

	/**
	 * Calculates the result of applying the "+" operator to the arguments from
	 * the stack and pushes it back on the stack.
	 */
	public void run(Stack stack) throws ParseException
	{
		checkStack(stack);// check the stack

		try
		{
			ClassLoader loader = this.getClass()
					.getClassLoader();

			int argCount = (curNumberOfParameters - 2) / 2;
			Object[] args = new Object[argCount];
			Class[] sig = new Class[argCount];
			int i = 3;
			int index = 0;

			// repeat summation for each one of the current parameters
			while (i < curNumberOfParameters)
			{
				// get the parameter from the stack
				Object arg = stack.pop();
				i++;
				String type = (String) stack.pop();
				i++;
				Class argClass = Util.getPrimitiveTypeClass(type);
				if (argClass == null)
				{
					argClass = loader.loadClass(type);
				}
				sig[argCount - index - 1] = argClass;
				args[argCount - index - 1] = Util.getFieldValueObject(argClass,
						arg);
				index++;
			}
			String methodName = (String) stack.pop();
			String className = "";
			Object classInstance = stack.pop();
			if(className.equals(""))
			{
				className = classInstance.getClass().getName();
			}
			
			Class clazz = null;
			clazz = loader.loadClass(className);
			java.lang.reflect.Method refMethod = Util
					.getClassMethod(clazz, methodName, sig);
			Object returnClazzInstance = refMethod.invoke(classInstance, args);
			stack.push(returnClazzInstance);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ParseException("Unable to load Class");
		}
		return;
	}
}
